#include "LCD.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


// When the display powers up, it is configured as follows:
//
// 1. Display clear
// 2. Function set: 
//    DL = 1; 8-bit interface data 
//    N = 0; 1-line display 
//    F = 0; 5x8 dot character font 
// 3. Display on/off control: 
//    D = 0; Display off 
//    C = 0; Cursor off 
//    B = 0; Blinking off 
// 4. Entry mode set: 
//    I/D = 1; Increment by 1 
//    S = 0; No shift 
//
// Note, however, that resetting the Arduino doesn't reset the LCD, so we
// can't assume that its in that state when a sketch starts (and the
// LiquidCrystal constructor is called).

// modification:
// don't use ports from Arduino, but use ports from Wire

// a nibble is a half Byte

// NEW: http://playground.arduino.cc//Code/LCDAPI
// NEW: setBacklight

//������ ��� �������� � ������� �����
unsigned char mass_Russian_simbol[2][66] = {
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�',
  0x41, 0xA0, 0x42, 0xA1, 0xE0, 0x45, 0xA2, 0xA3, 0xA4, 
  0xA5, 0xA6, 0x4B, 0xA7, 0x4D, 0x48, 0x4F, 0xA8, 0x50,
  0x43, 0x54, 0xA9, 0xAA, 0x58, 0xE1, 0xAB, 0xAC, 0xE2, 
  0xAD, 0xAE, 0x62, 0xAF, 0xB0, 0xB1, 0x61, 0xB2, 0xB3, 
  0xB4, 0xE3, 0x65, 0xB5, 0xB6, 0xB7, 0xB8, 0xA6, 0xBA, 
  0xBB, 0xBC, 0xBD, 0x6F, 0xBE, 0x70, 0x63, 0xBF, 0x79, 
  0xE4, 0x78, 0xE5, 0xC0, 0xC1, 0xE6, 0xC2, 0xC3, 0xC4,
  0xC5, 0xC6, 0xC7
};


/*==================================================*/
/*====================������ � LCD==================*/
/*==================================================*/
// NEW:
uint8_t _Addr;        ///< Wire Address of the LCD
uint8_t _backlight;   ///< the backlight intensity 

uint8_t _displayfunction; ///< lines and dots mode
uint8_t _displaycontrol;  ///< cursor, display, blink flags
uint8_t _displaymode;     ///< left2right, autoscroll

uint8_t _numlines;        ///< The number of rows the display supports.

I2C_HandleTypeDef* hi2c = NULL;

void I2C_Scan(UART_HandleTypeDef* huart) {
  char info[] = "Scanning I2C bus...\r\n";
  HAL_UART_Transmit(huart, (uint8_t*)info, strlen(info),
                    HAL_MAX_DELAY);
  
  HAL_StatusTypeDef res;
  for(uint16_t i = 0; i < 128; i++) {
    res = HAL_I2C_IsDeviceReady(hi2c, i << 1, 1, 10);
    if(res == HAL_OK) {
      char msg[64];
      snprintf(msg, sizeof(msg), "0x%02X", i); 
      HAL_UART_Transmit(huart, (uint8_t*)msg, strlen(msg),
                        HAL_MAX_DELAY);
    } else {
      HAL_UART_Transmit(huart, (uint8_t*)".", 1,
                        HAL_MAX_DELAY);
    }
  }   
  
  HAL_UART_Transmit(huart, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
}

//������� ������� �������� � ��� LCD
unsigned int searh_russ_code(unsigned int simbol)
{
  for (unsigned int i = 0; i < 66; i++)
  {
    if (mass_Russian_simbol[0][i] == simbol)
      return mass_Russian_simbol[1][i];
  }
  return 0xFF;
}

//����� ������ �� �������(������� �������)
void LCD_write(uint8_t value)
{
  _send(value, RSMODE_DATA);
  /*int i = 0;
  while (string[i] != '\0')
  {
    // uint8_t code = 0;
    // code = searh_russ_code(string[i]);
    // if(code == 0xFF) send_data(string[i]);
    // else send_data(code);
    i++;
  }*/
}

void LCD_print(uint8_t *string)
{
  int i = 0;
  while (string[i] != '\0')
  {
    uint8_t code = 0;
    code = searh_russ_code(string[i]);
    if(code == 0xFF) 
      _send(string[i], RSMODE_DATA);
    else 
      _send(code, RSMODE_DATA);
    i++;
  }
}

//��������
void delayMicroseconds(uint32_t micros) {
  micros = micros*36*2*4;//���������� ������
  for( ; micros != 0; micros--) {
    __NOP(); //1 ���� ���������� ����� 0.13 ����
  }
}


/*===========================================================================*/
void LCD_Init(I2C_HandleTypeDef* hi2cInit, uint8_t addr)
{
  _Addr = addr;
  _backlight = 0;
  hi2c = hi2cInit;
  begin(0, 2, LCD_5x8DOTS);
} // LiquidCrystal_PCF8574


void begin(uint8_t cols, uint8_t lines, uint8_t dotsize) {
  // cols ignored !
  
  _numlines = lines;

  _displayfunction = 0;

  if (lines > 1) {
    _displayfunction |= LCD_2LINE;
  }

  // for some 1 line displays you can select a 10 pixel high font
  if ((dotsize != 0) && (lines == 1)) {
    _displayfunction |= LCD_5x10DOTS;
  }

  // SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
  // according to datasheet, we need at least 40ms after power rises above 2.7V
  // before sending commands. Arduino can turn on way befor 4.5V so we'll wait 50

  // initializing th display
  _write2Wire(0x00, LOW, false);
  delayMicroseconds(50000); 

  // put the LCD into 4 bit mode according to the hitachi HD44780 datasheet figure 26, pg 47
  _sendNibble(0x03, RSMODE_CMD);
  delayMicroseconds(4500); 
  _sendNibble(0x03, RSMODE_CMD);
  delayMicroseconds(4500); 
  _sendNibble(0x03, RSMODE_CMD);
  delayMicroseconds(150);
  // finally, set to 4-bit interface
  _sendNibble(0x02, RSMODE_CMD);

  // finally, set # lines, font size, etc.
  _command(LCD_FUNCTIONSET | _displayfunction);  

  // turn the display on with no cursor or blinking default
  _displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;  
  display();

  // clear it off
  clear();

  // Initialize to default text direction (for romance languages)
  _displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
  // set the entry mode
  _command(LCD_ENTRYMODESET | _displaymode);
}

/********** high level commands, for the user! */
void clear()
{
  _command(LCD_CLEARDISPLAY);  // clear display, set cursor position to zero
  delayMicroseconds(2000);  // this command takes a long time!
}

void home()
{
  _command(LCD_RETURNHOME);  // set cursor position to zero
  delayMicroseconds(2000);  // this command takes a long time!
}


/// Set the cursor to a new position. 
void setCursor(uint8_t col, uint8_t row)
{
  int row_offsets[] = { 0x00, 0x40, 0x14, 0x54   };
  if ( row >= _numlines ) {
    row = _numlines-1;    // we count rows starting w/0
  }

  _command(LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

// Turn the display on/off (quickly)
void noDisplay() {
  _displaycontrol &= ~LCD_DISPLAYON;
  _command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void display() {
  _displaycontrol |= LCD_DISPLAYON;
  _command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turns the underline cursor on/off
void noCursor() {
  _displaycontrol &= ~LCD_CURSORON;
  _command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void cursor() {
  _displaycontrol |= LCD_CURSORON;
  _command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turn on and off the blinking cursor
void noBlink() {
  _displaycontrol &= ~LCD_BLINKON;
  _command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void blink() {
  _displaycontrol |= LCD_BLINKON;
  _command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// These commands scroll the display without changing the RAM
void scrollDisplayLeft(void) {
  _command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}
void scrollDisplayRight(void) {
  _command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

// This is for text that flows Left to Right
void leftToRight(void) {
  _displaymode |= LCD_ENTRYLEFT;
  _command(LCD_ENTRYMODESET | _displaymode);
}

// This is for text that flows Right to Left
void rightToLeft(void) {
  _displaymode &= ~LCD_ENTRYLEFT;
  _command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'right justify' text from the cursor
void autoscroll(void) {
  _displaymode |= LCD_ENTRYSHIFTINCREMENT;
  _command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'left justify' text from the cursor
void noAutoscroll(void) {
  _displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
  _command(LCD_ENTRYMODESET | _displaymode);
}


/// Setting the brightness of the background display light.
/// The backlight can be switched on and off.
/// The current brightness is stored in the private _backlight variable to have it available for further data transfers.
void setBacklight(uint8_t brightness) {
  _backlight = brightness;
  // send no data but set the background-pin right;
  _write2Wire(0x00, RSMODE_DATA, false);
} // setBacklight


// Allows us to fill the first 8 CGRAM locations
// with custom characters
void createChar(uint8_t location, uint8_t charmap[]) {
  location &= 0x7; // we only have 8 locations 0-7
  _command(LCD_SETCGRAMADDR | (location << 3));
  for (int i=0; i<8; i++) {
    // write(charmap[i]);
  }
}


/* ----- low level functions ----- */

inline void _command(uint8_t value) {
  _send(value, RSMODE_CMD);
} // _command()


// write either command or data
void _send(uint8_t value, uint8_t mode) {
  // separate the 4 value-nibbles
  uint8_t valueLo = value    & 0x0F;
  uint8_t valueHi = value>>4 & 0x0F;

  _sendNibble(valueHi, mode);
  _sendNibble(valueLo, mode);
} // _send()


// write a nibble / halfByte with handshake
void _sendNibble(uint8_t halfByte, uint8_t mode) {
  _write2Wire(halfByte, mode, true);
  delayMicroseconds(1);    // enable pulse must be >450ns
  _write2Wire(halfByte, mode, false);
  delayMicroseconds(37);   // commands need > 37us to settle
} // _sendNibble


// private function to change the PCF8674 pins to the given value
void _write2Wire(uint8_t halfByte, uint8_t mode, uint8_t enable) {
  // map the given values to the hardware of the I2C schema
  
  uint8_t i2cData = halfByte << 4;
  if (mode > 0) i2cData |= PCF_RS;
  // PCF_RW is never used.
  if (enable > 0) i2cData |= PCF_EN;
  if (_backlight > 0) i2cData |= PCF_BACKLIGHT;

  HAL_I2C_Master_Transmit(hi2c, (_Addr << 1),&i2cData, 1, HAL_MAX_DELAY); 
} // write2Wire




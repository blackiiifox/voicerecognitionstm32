#include "commands.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h> 

//=========================================================================================================
//=============================================������ � �������============================================
//=========================================================================================================
//������������ ��������
StructCommand* CreateCommand(char* name, char* date)
{
  StructCommand* new_command = malloc(sizeof(StructCommand));
  new_command->name_command = name;
  new_command->date_command = date;
  return new_command;
}

void DeleteCommand(StructCommand* command)
{
  free(command->name_command);
  free(command->date_command);
  free(command);
}

//==============================================================================
//=========================LIST COMMANDS========================================
//==============================================================================

//Add new commmand in list
void AppendCommand(ListCommands* listCommand,StructCommand* newCommand)
{
  while(!listCommand->next_addr==NULL)
  {
    listCommand = (ListCommands*)listCommand->next_addr;
  }
  
  if(listCommand->command == NULL)
  {
    listCommand->command = newCommand;
  }
  else
  {
    ListCommands* new_elem_list = malloc(sizeof(ListCommands));
    new_elem_list->command = newCommand;
    new_elem_list->next_addr = NULL;
    listCommand->next_addr = (unsigned int*)new_elem_list;
  }
}


//Get command by name
StructCommand* GetCommand(ListCommands* listCommand, char* nameCommand)
{
  StructCommand* command = NULL;
  while(1)
  {
    if(strcmp (listCommand->command->name_command, nameCommand) == 0)
    {
      command = listCommand->command;
      return command;
    }
    
    if (listCommand->next_addr==NULL)
      break;
    
    listCommand = (ListCommands*)listCommand->next_addr;
  }
  return NULL;
}

// Get command by id
StructCommand* GetCommandById(ListCommands* listCommand, uint32_t id)
{
  StructCommand* command = NULL;
  
  uint32_t count_command = ShowCountCommand(listCommand);
  if (id > count_command)
    return command;
  
  for (int i = 0; i < id; i++)
  {
    listCommand = (ListCommands*)listCommand->next_addr;
  }
  command = listCommand->command;
  return command;
}


//Show count commands in list
unsigned int ShowCountCommand(ListCommands* listCommand)
{
  unsigned int count_command = 0;
  if (listCommand != NULL)
  {
    while(1)
    {
      count_command++;
      
      if (listCommand->next_addr==NULL)
        break;
      else
        listCommand = (ListCommands*)listCommand->next_addr;
      
    }
  }
  return count_command;
  //return Data_list->data;
}

//Delete list
void DeleateListCommands(ListCommands* listCommand)
{
  if (listCommand == NULL)
    return;
  while(1)
  { 
    if (listCommand->command != NULL)
    {
      DeleteCommand(listCommand->command);
      listCommand->command = NULL; 
    }
    ListCommands* prev_addr = (ListCommands*)listCommand;
    listCommand = (ListCommands*)listCommand->next_addr;
    free(prev_addr);
    
    if (listCommand == NULL)
      break;
  }
}

//Create new list commands
ListCommands* CreateListCommands()
{
  ListCommands* new_list = malloc(sizeof(ListCommands));
  new_list->command = NULL;
  new_list->next_addr = NULL;
  return new_list;
}

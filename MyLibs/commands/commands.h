#ifndef commands
#define commands
#include "stm32f7xx_hal.h"

//commands.h usage example 
//ListCommands* listCommands = CreateListCommands();
//
//char *name1 = malloc(sizeof(char)*20);
//name1[0] = 'a';
//name1[1] = 'b';
//for (int i = 2; i < 20; i++)
//{
//  name1[i] = 0;
//}
//StructCommand* command = CreateCommand(name1);
//command->info.count_actions = 3;
//for (int i = 0; i < command->info.count_actions; i++)
//{
//  StructAction *action = CreateAction();
//  command->addr_actions[i] = (uint32_t)action;
//}
//StructAction *action = (StructAction*)command->addr_actions[0];
//
//AppendCommand(listCommands,command);
//
//char *name2 = malloc(sizeof(char)*20);
//name2[0] = '1';
//name2[1] = '4';
//for (int i = 2; i < 20; i++)
//{
//  name2[i] = 0;
//}
//command = CreateCommand(name2);
//command->info.count_actions = 2;
//for (int i = 0; i < command->info.count_actions; i++)
//{
//  StructAction *action = CreateAction();
//  command->addr_actions[i] = (uint32_t)action;
//}
//
//AppendCommand(listCommands,command);
//
//char *name3 = malloc(sizeof(char)*20);
//name3[0] = '1';
//name3[1] = '4';
//for (int i = 2; i < 20; i++)
//{
//  name3[i] = 0;
//}
//StructCommand* curremtCommand = GetCommand(listCommands, name3);
//DeleateListCommands(listCommands);

//==============================================================================
//============================Data Commands=====================================
//==============================================================================

typedef struct{
  char *name_command;
  char *date_command;
} StructCommand;

//API
StructCommand* CreateCommand(char* name, char* date);
void DeleteCommand(StructCommand* command);

//==============================================================================
//============================List Commands=====================================
//==============================================================================
typedef struct{
  uint32_t* next_addr;
  StructCommand* command;
} ListCommands;

//API
ListCommands* CreateListCommands(); //�������� ������ ������
void DeleateListCommands(ListCommands* listCommand); //���������� ������
void AppendCommand(ListCommands* listCommand,StructCommand* newCommand); //�������� 
StructCommand* GetCommand(ListCommands* listCommand, char* nameCommand); // Get command by name
StructCommand* GetCommandById(ListCommands* listCommand, uint32_t id);      // Get command by id
unsigned int ShowCountCommand(ListCommands* listCommand); //����� ���-�� ���������

#endif
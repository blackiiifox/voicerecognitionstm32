#include "file_system.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h> 

//==============================================================================
//============================File system work==================================
//==============================================================================
HAL_StatusTypeDef       flash_ok = HAL_ERROR;
void SaveAllCommands(ListCommands* listCommands)
{
  ClearCommandFlash();
  
  // Save commands
  flash_ok = HAL_ERROR;
  while(flash_ok != HAL_OK){
    flash_ok = HAL_FLASH_Unlock();
  }
  
  
  StructCommand* command = NULL;
  uint32_t count_command =  ShowCountCommand(listCommands);
  uint32_t AddressPage = StartPage;
  uint32_t offsetIndexCommand = 0;
  
  
  
  for (uint32_t indexCommand = 0; indexCommand < count_command; indexCommand++)
  {
    uint32_t address = AddressPage + (32 * offsetIndexCommand);
    
    command =  GetCommandById(listCommands, indexCommand);
    if (command == NULL)
    {
      continue;
    }
    offsetIndexCommand++;
    
    
    uint32_t base_address_name = address;
    for (int i = 0; i < 5; i++)
    {
      // Convert name
      uint32_t name;
      name = command->name_command[i*4];
      name |= command->name_command[i*4 + 1] << 8;
      name |= command->name_command[i*4 + 2] << 16;
      name |= command->name_command[i*4 + 3] << 24;
      // Save name
      uint32_t address_byte = base_address_name + (4*i);
      
      flash_ok = HAL_ERROR;
      while(flash_ok != HAL_OK){
        flash_ok = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address_byte, name);
      }	
    }
    
    // Save date
    uint32_t base_address_date = address + 0x14;
    for (int i = 0; i < 3; i++)
    {
      // Convert date
      uint32_t date;
      date = command->date_command[i*4];
      date |= command->date_command[i*4 + 1] << 8;
      date |= command->date_command[i*4 + 2] << 16;
      date |= command->date_command[i*4 + 3] << 24;
      
      // Save date
      uint32_t address_byte = base_address_date + (4*i);
      flash_ok = HAL_ERROR;
      while(flash_ok != HAL_OK){
        flash_ok = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address_byte, date);
      }	
    }
    
  }
  
  flash_ok = HAL_ERROR;
  while(flash_ok != HAL_OK){
    flash_ok = HAL_FLASH_Lock();
  }
}

ListCommands* LoadAllCommands()
{
  ListCommands* listCommands = CreateListCommands();
  StructCommand* command;
  
  for(int offsetIndexCommand = 0;;offsetIndexCommand++)
  {
    uint32_t address_command = StartPage + (32 * offsetIndexCommand);
    char *nameCommandInSystem = malloc(sizeof(char)*20);
    LoadString(nameCommandInSystem, (char*)address_command, 20);
    
    if(nameCommandInSystem[0] == 0xFF && nameCommandInSystem[1] == 0xFF)
    {
      free(nameCommandInSystem);
      break;
    }
    
    char *dateCommandInSystem = malloc(sizeof(char)*12);
    LoadString(dateCommandInSystem, (char*)(address_command + 0x14), 12);
    
    command = CreateCommand(nameCommandInSystem, dateCommandInSystem);
    AppendCommand(listCommands, command);
  }
  return listCommands;
}

StructCommand* LoadCommand(char* nameCommand)
{
  StructCommand* command = NULL;
  char *nameCommandInSystem = malloc(sizeof(char)*20);
  // char nameCommandInSystem[20];
  
  uint32_t AddressPage = StartPage;
  uint32_t num_page = 0;
  uint32_t offsetIndexCommand = 0;
  
  for (int indexCommand = 0; indexCommand < SizePage * 5; indexCommand++)
  {
    
    if (indexCommand%4 == 0 && indexCommand!=0)
    {
      num_page++;
      if(SizePage == num_page)
        break;
      AddressPage = StartPage + (1024 * num_page);
      offsetIndexCommand = 0;
    }
    uint32_t address_command = AddressPage + (200 * offsetIndexCommand);
    
    offsetIndexCommand++;
    
    LoadString(nameCommandInSystem, (char*)address_command, 20);
    
    if(strcmp (nameCommandInSystem, nameCommand) == 0)
    {
      // info load
      char *dateCommandInSystem = malloc(sizeof(char)*12);
      LoadString(dateCommandInSystem, (char*)(address_command + 0x14), 12);
      
      command = CreateCommand(nameCommandInSystem, dateCommandInSystem);
      
      return command;
    }
  }
  // if not finde delete char array and return null
  free(nameCommandInSystem);
  return command;
}

void LoadString(char* name, char* address, uint32_t size)
{
  for (int i = 0; i < 20; i++)
  {
    name[i] = *(address + i);
  }
}

StructCommand* ConvertArrayToCommand(uint32_t* array)
{
  StructCommand* command = NULL;
  uint32_t address_command = (uint32_t)array;
  
  char *nameCommandInSystem = malloc(sizeof(char)*20);
  LoadString(nameCommandInSystem, (char*)address_command, 20);
  
  char *dateCommandInSystem = malloc(sizeof(char)*12);
  LoadString(dateCommandInSystem, (char*)(address_command + 0x14), 12);
  
  command = CreateCommand(nameCommandInSystem, dateCommandInSystem);
  
  return command;
}

void ClearCommandFlash()
{
  // Clear page in flash
  uint32_t PAGEError = 0;
  flash_ok = HAL_ERROR;
  while(flash_ok != HAL_OK){
    flash_ok = HAL_FLASH_Unlock();
  }
  
  FLASH_EraseInitTypeDef pEraseInit;
  pEraseInit.TypeErase = FLASH_TYPEERASE_SECTORS;
  pEraseInit.Sector = FLASH_SECTOR_8; 
  pEraseInit.NbSectors = 1;
  pEraseInit.VoltageRange = FLASH_VOLTAGE_RANGE_3;
  
  HAL_Delay(50);
  flash_ok = HAL_ERROR;
  flash_ok = HAL_FLASHEx_Erase(&pEraseInit, &PAGEError);
  HAL_Delay(50);
  
  flash_ok = HAL_ERROR;
  while(flash_ok != HAL_OK){
    flash_ok = HAL_FLASH_Lock();
  }
}
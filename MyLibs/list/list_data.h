#ifndef list_data
#define list_data



//=========================================================================================================
//=============================================������ � �������============================================
//=========================================================================================================
typedef struct{
  unsigned int* next_addr;
  unsigned char data;
} StructData;

//API
StructData* Create_Data_list(); //�������� ������ ������
void Deleate_list(StructData* Data_list); //���������� ������
void Append_Data(StructData* Data_list,unsigned char data); //�������� 
unsigned char* Get_Data(StructData* Data_list); //����� ���� ������� ��������� � ������
unsigned char Show_Count_Data(StructData* Data_list); //����� ���-�� ���������


//���������� �������
void InitListData(StructData* Data); //��������������� ���������






//=========================================================================================================
//==========================================������ � �����������===========================================
//=========================================================================================================
//��� ���������� � ������� ������
typedef struct{
  StructData* current_addr; //������� ������������ ����� ��� ������
  unsigned char info;         //��������� �������(0-��������� ������� ����� ������ ������, 1-������� ������ ��� ��� �����������)
  int countlost;
} StuctInfoData;


//API
StuctInfoData* Create_Info_Data(); //������� ��������� ��� �������� ����������
void InfoDataSet(StuctInfoData* info,StructData* list_data,int countdata);//��� �������� ������ ������ ������
unsigned char ShowInfo(StuctInfoData* info); //�������� ���������� � ������� ������������� ������ ������
StructData* Get_List(StuctInfoData* info);//�������� ������������ ������ ������
unsigned char Check_End_Receive(StuctInfoData* info);//�������� ��������� ������

//���������� �������
void InfoDataClear(StuctInfoData* info); //��� ��������� ������ ������
void InitStuctInfoData(StuctInfoData* info);//��������������� ���������
#endif
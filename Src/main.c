
/**
******************************************************************************
* @file           : main.c
* @brief          : Main program body
******************************************************************************
** This notice applies to any and all portions of this file
* that are not between comment pairs USER CODE BEGIN and
* USER CODE END. Other portions of this file, whether 
* inserted by the user or by software development tools
* are owned by their respective copyright owners.
*
* COPYRIGHT(c) 2018 STMicroelectronics
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of STMicroelectronics nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f7xx_hal.h"
#include "i2c.h"
#include "i2s.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "LCD.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "data_handler.h"
#include "file_system.h"
#include "commands.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
// Program defines
#define Frequency 8000          // Freq ADC
#define TimeRecord 2            // Time record speech
#define BytePerSample 2         // Byte in single sample
// System commands
#define CommandSaveToVoice 0x19
// ������ ������
#define VoiceHandler 0x01
#define ButtonHandler 0x02

// Logic program
char StateSendWav;
char StateRecWav = 0;
uint8_t currentOperationMode = 0; 

// Variables
unsigned char pRxData[1];

// Wav data variables
typedef struct{
  uint32_t chunkId;        //�RIFF� in ASCII
  uint32_t chunkSize;            //Remaining size
  uint32_t format;        //�WAVE� in ASCII
  uint32_t subchunk1Id;    //�fmt� in ASCII
  uint32_t subchunk1Size;               //16 in PCM format
  uint16_t audioFormat;             //For PCM = 1
  uint16_t numChannels;             //Mono channel
  uint32_t sampleRateWav;        //Sampling frequency
  uint32_t byteRate;            //Byte Rate
  uint16_t blockAlign;              //Byte per sample 
  uint16_t bitsPerSample;          //Bits per sample 
  uint32_t subchunk2Id;    //�data� in ASCII
  uint32_t subchunk2Size;               //Remaining size
  float* data;                       //WAV data in float point
} StructWavHeader;

StructWavHeader WavHeader;              // Header
uint32_t WavData[Frequency * TimeRecord];  // Wav Data buffer
int currentIndex;
char stateWav;

// Receive
uint8_t dataRx;
StructPackageBuffer receivePackage;
uint32_t time_last_receive = 0;

// Commands
ListCommands* listCommands = NULL;
StructCommand* currentCommand = NULL;
uint32_t currentIndexCommand = 0;
uint32_t time_last_push = 0;
uint16_t LastPushButton = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void InitWaveStructure();
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
//==============================================================================
//============================== Functions =====================================
//==============================================================================
void InitWaveStructure(StructWavHeader *init_stuctur);  // ������������ wav ��������� �������������� ������
void SendWavFile();                                     // ������������ WAV ������ � ��������
void RecognizePackage(StructPackage* package);          // ��������� ����������� ������
void ConvertPackageToListCommands(StructPackage* package); // �������������� �������� ������ � ������ ������
void setCurrentCommand(StructCommand* newCurrent);      // ������������� ����� ������� �������
void PushButtonEnter();                                 // ���������� ��������� ������� ������ �����
void PushButtonDown();                                  // ���������� ��������� ������� ������ ����
void PushBottonUp();                                    // ���������� ��������� ������� ������ �����
void setOperationMode(uint8_t newMode);                 // ��������� ������ ������ ������
void setBaseDisplay(uint8_t mode);                      // ����� �� ������� ����������, ��������� �� ������ ������
void PrintCommand(char* name);                          // ����� �������� �� ������ ������ �������

//==============================================================================
//============================= Interrupts =====================================
//==============================================================================
// Tim 8000 Hz
// Start ADC conversation
uint16_t data_in[2];
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if (currentIndex == Frequency * TimeRecord - 1)
  {
    HAL_TIM_Base_Stop_IT(&htim3);
    stateWav = 1;
    currentIndex = 0;
    HAL_NVIC_EnableIRQ(EXTI0_IRQn);
  }
  else
  {
    HAL_I2S_Receive_IT(&hi2s2, data_in, 2);
    // HAL_ADC_Start_IT(&hadc1);
    
  }
  
}

void HAL_I2S_RxCpltCallback (I2S_HandleTypeDef * hi2s)
{
  if(StateRecWav != 0)
  {
    if (currentIndex == Frequency * TimeRecord - 1)
    {
      StateRecWav = 0;
      HAL_TIM_Base_Stop_IT(&htim3);
      stateWav = 1;
      currentIndex = 0;
      HAL_NVIC_EnableIRQ(EXTI0_IRQn);
    }
    else
    {
      volatile int32_t data_full = (int32_t) data_in [0] << 16 | data_in [1]; 
      // volatile int16_t data_full = (int16_t) data_in [0];
      WavData[currentIndex] = (uint32_t)data_full;
      currentIndex++;
      // HAL_ADC_Start_IT(&hadc1);
    }
  }
  HAL_I2S_Receive_IT(&hi2s2, data_in, 2);
}

void HAL_I2S_ErrorCallback(I2S_HandleTypeDef * hi2s)
{
  // currentIndex++;
}

// USART Receive interrupts
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if (huart == &huart3)
  {
    uint32_t time = HAL_GetTick();
    if((time - time_last_receive) > 1000)
    {
      Clear_Data_Receive();
    }
    Append_Data_Receive(dataRx);
    // add_data_to_buffer(&buffer_input, dataRx);
    time_last_receive = time;
    HAL_UART_Receive_IT(&huart3, &dataRx, 1);
  }
}

// GPIO push 
// Start voice
void HAL_GPIO_EXTI_Callback (uint16_t GPIO_Pin)
{
  uint32_t time = HAL_GetTick();
  uint32_t prev_push = time_last_push;
  time_last_push = time;
  
  if((time - prev_push) < 200)
    {
      if (((LastPushButton == GPIO_PIN_9) && (GPIO_Pin == GPIO_PIN_10)) || 
          ((LastPushButton == GPIO_PIN_10) && (GPIO_Pin == GPIO_PIN_9)))
      {
        if(currentOperationMode == VoiceHandler)
          currentOperationMode = ButtonHandler;
        else
          currentOperationMode = VoiceHandler;
        setOperationMode(currentOperationMode);
        // ChangeMode();
      }
      
       return;
    }
  
  if(GPIO_Pin== GPIO_PIN_8) {
    PushButtonEnter();
    // HAL_NVIC_DisableIRQ(EXTI0_IRQn);
    // StateRecWav = 1;
  }
  
  if(GPIO_Pin== GPIO_PIN_9) {
    PushButtonDown();
  }
  
  if(GPIO_Pin== GPIO_PIN_10) {
    PushBottonUp();
  }
  LastPushButton = GPIO_Pin;
}


//==============================================================================
//============================== Functions =====================================
//==============================================================================
void PushButtonEnter()
{
  
}

void PushButtonDown()
{
  switch(currentOperationMode)
  {
  case ButtonHandler:
    {
      if (currentIndexCommand != 0)
        currentIndexCommand--;  
      else
        currentIndexCommand = ShowCountCommand(listCommands) - 1;
      
      currentCommand = GetCommandById(listCommands, currentIndexCommand);
      
      if(currentCommand != NULL)
        PrintCommand(currentCommand->name_command);
      break;
    }
  }
}

void PushBottonUp()
{
  switch(currentOperationMode)
  {
  case ButtonHandler:
    {
      if (ShowCountCommand(listCommands) > currentIndexCommand+1)
        currentIndexCommand++;  
      else
        currentIndexCommand = 0;
      
      currentCommand = GetCommandById(listCommands, currentIndexCommand);
      
      if(currentCommand != NULL)
        PrintCommand(currentCommand->name_command);
      break;
    }
  }
}

void setOperationMode(uint8_t newMode)
{
  currentOperationMode = newMode;
  setBaseDisplay(currentOperationMode);
}

void PrintCommand(char* name)
{
  setCursor(0,1);
  LCD_print("                ");
  setCursor(0,1);
  LCD_print((uint8_t*)name);
}

void setBaseDisplay(uint8_t mode)
{
  clear();
  switch(mode)
  {
  case VoiceHandler:
    {
      setCursor(0,0);
      LCD_print("��������� ���.:");
      break;
    }
    
  case ButtonHandler:
    {
      setCursor(0,0);
      LCD_print("�������:");
      setCursor(0,1);
      if(currentCommand != NULL)
        LCD_print((uint8_t*)(currentCommand->name_command));
      break;
    }
  }
}

// ������������� ����� ������� �������
void setCurrentCommand(StructCommand* newCurrent)
{
  currentCommand = newCurrent;
  char* nameCommand = currentCommand->name_command;
  setCursor(0,1);
  LCD_print((uint8_t*)nameCommand);
}

// ��������� ��������� ������
void RecognizePackage(StructPackage* recPackage)
{
  switch ( recPackage->package[0] ) {
  case CommandSaveToVoice:
    {
      ConvertPackageToListCommands(recPackage);
      break;
    }
  }
  
  free(recPackage->package);
  free(recPackage);
}

// �������������� ������ � ������ ����� + ���� �������
void ConvertPackageToListCommands(StructPackage* package)
{
  int index = 1;
  if(package->countData > 1)
  {
    ListCommands* listCommands = CreateListCommands();
    while (index < package->countData)
    {
      StructCommand* newCommand = ConvertArrayToCommand((uint32_t*)&(package->package[index]));
      AppendCommand(listCommands, newCommand);
      index = index + 32;
    }
    SaveAllCommands(listCommands);
    DeleateListCommands(listCommands);
  }
}

// Initialize wave structure
void InitWaveStructure(StructWavHeader *init_stuctur)
{
  // Static
  init_stuctur->chunkId = 0x52494646;        //"RIFF" in ASCII
  init_stuctur->format = 0x57415645 ;        //�WAVE� in ASCII
  init_stuctur->subchunk1Id = 0x666d7420;    //�fmt� in ASCII
  init_stuctur->subchunk1Size = 16;          //16 in PCM format 
  init_stuctur->audioFormat = 1;             //For PCM = 1
  init_stuctur->numChannels = 1;             //Mono channel
  init_stuctur->subchunk2Id = 0x64617461;    //�data� in ASCII
  
  // Unstatic
  init_stuctur->bitsPerSample = BytePerSample * 8;      //Bits per sample
  init_stuctur->blockAlign = BytePerSample ;            //Byte per sample 
  init_stuctur->sampleRateWav = Frequency;              //Sampling frequency
  init_stuctur->byteRate = BytePerSample * Frequency;   //Byte Rate 
  
  init_stuctur->chunkSize = init_stuctur->byteRate * TimeRecord + 36;  //Remaining size after chunkId
  init_stuctur->subchunk2Size = init_stuctur->byteRate * TimeRecord;                 //Remaining size afted chank1
}

// SendWavFile
void SendWavFile()
{
  
  char headerByte[44];
  int index = 0;
  for ( int i = 3; i >= 0; i-- )
  {
    headerByte[index] = (char)(( WavHeader.chunkId >> (i * 8) ) & 0xFF);
    index++;
  }
  for ( int i = 0; i < 4; i++ )
  {
    headerByte[index] = (char)(( WavHeader.chunkSize >> (i * 8) ) & 0xFF);
    index++;
  }
  
  for ( int i = 3; i >= 0; i-- )
  {
    headerByte[index] =  (char) (( WavHeader.format >> (i * 8) ) & 0xFF);
    index++;
  }
  
  for ( int i = 3; i >= 0; i-- )
  {
    headerByte[index] = (char) ( ( WavHeader.subchunk1Id >> (i * 8) ) & 0xFF);
    index++;
  }
  
  for ( int i = 0; i < 4; i++ )
  {
    headerByte[index] = (char) ( ( WavHeader.subchunk1Size >> (i * 8) ) & 0xFF);
    index++;
  }
  
  for ( int i = 0; i < 2; i++ )
  {
    headerByte[index] = (char) ( ( WavHeader.audioFormat >> (i * 8) ) & 0xFF);
    index++;
  }
  
  for ( int i = 0; i < 2; i++ )
  {
    headerByte[index] = (char) ( ( WavHeader.numChannels >> (i * 8) ) & 0xFF);
    index++;
  }
  
  for ( int i = 0; i < 4; i++ )
  {
    headerByte[index] = (char) ( ( WavHeader.sampleRateWav >> (i * 8) ) & 0xFF);
    index++;
  }
  
  for ( int i = 0; i < 4; i++ )
  {
    headerByte[index] = (char) ( ( WavHeader.byteRate >> (i * 8) ) & 0xFF);
    index++;
  }
  
  for ( int i = 0; i < 2; i++ )
  {
    headerByte[index] = (char) ( ( WavHeader.blockAlign >> (i * 8) ) & 0xFF);
    index++;
  }
  
  for ( int i = 0; i < 2; i++ )
  {
    headerByte[index] = (char) ( ( WavHeader.bitsPerSample >> (i * 8) ) & 0xFF);
    index++;
  }
  
  for ( int i = 3; i >= 0; i-- )
  {
    headerByte[index] = (char) ( ( WavHeader.subchunk2Id >> (i * 8) ) & 0xFF);
    index++;
  }
  
  for ( int i = 0; i < 4; i++ )
  {
    headerByte[index] = (char) ( ( WavHeader.subchunk2Size >> (i * 8) ) & 0xFF);
    index++;
  }
  
  HAL_UART_Transmit(&huart3, (uint8_t*)headerByte, 44,0xFFFF);
  
  for (int i = 0; i < Frequency * TimeRecord; i++)
  {
    uint8_t short_data_sample;
    uint32_t data = WavData[i];
    //    short_data_sample = data & 0x000000FF;
    //    HAL_UART_Transmit(&huart3, &short_data_sample, 1,0xFFFF);
    //    
    //    short_data_sample = (data & 0x0000FF00) >> 8;
    //    HAL_UART_Transmit(&huart3, &short_data_sample, 1,0xFFFF);
    //    
    //    short_data_sample = (data & 0x00FF0000) >> 16;
    //    HAL_UART_Transmit(&huart3, &short_data_sample, 1,0xFFFF);
    
    // short_data_sample = (data & 0x0000FF00) >> 8;
    // HAL_UART_Transmit(&huart3, &short_data_sample, 1,0xFFFF);
    
    short_data_sample = (data & 0x00FF0000) >> 16;
    HAL_UART_Transmit(&huart3, &short_data_sample, 1,0xFFFF);
    
    short_data_sample = (data & 0xFF000000) >> 24;
    HAL_UART_Transmit(&huart3, &short_data_sample, 1,0xFFFF);
    
    
    //    short_data_sample = (data & 0xFF000000) >> 24;
    //    HAL_UART_Transmit(&huart3, &short_data_sample, 1,0xFFFF);
  }
  
  stateWav = 0;
}
/* USER CODE END 0 */

/**
* @brief  The application entry point.
*
* @retval None
*/
int main(void)
{
  /* USER CODE BEGIN 1 */
  //__HAL_DBGMCU_UNFREEZE_IWDG (); // Delete this later
  /* USER CODE END 1 */
  
  /* MCU Configuration----------------------------------------------------------*/
  
  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();
  
  /* USER CODE BEGIN Init */
  
  /* USER CODE END Init */
  
  /* Configure the system clock */
  SystemClock_Config();
  
  /* USER CODE BEGIN SysInit */
  
  /* USER CODE END SysInit */
  
  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM3_Init();
  MX_USART3_UART_Init();
  MX_USART2_UART_Init();
  MX_I2S2_Init();
  MX_I2C2_Init();
  /* USER CODE BEGIN 2 */
  //============================================================================
  //=============================Init modules===================================
  //============================================================================
  Create_Data_Receive();        // ������� ����������� ���������
  CreatePackageBuffer(&receivePackage); // ������� ����� �������
  SetPackageBuf(&receivePackage);       // ������������� ����������� ��������� ����� ���������� �� ������
  
  InitWaveStructure(&WavHeader); // �������� ��������� ������������ WAV ���������
  LCD_Init(&hi2c2, 0x27);       // �������� �������
  HAL_UART_Receive_IT(&huart3, &dataRx, 1); // ������� ������ ����
  
  listCommands = LoadAllCommands();      // ��������� �������, ����������� � ������
  currentCommand = GetCommandById(listCommands, currentIndexCommand); // ������������� ������� ��������� ������� ��� ������ ������ � ��������
  setOperationMode(ButtonHandler);
  //============================================================================
  // setBacklight(1);
  // I2C_Scan(&huart3);
  // lcd_send(1,0xA0);
  // send_data(0xA0);
  
  // Init variablse
  currentIndex = 0;
  stateWav = 0;
  StateSendWav = 1;
  StateRecWav = 0;
  /* USER CODE END 2 */
  
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  //HAL_TIM_Base_Start_IT(&htim3);
  
  HAL_I2S_Receive_IT(&hi2s2, data_in, 2);
  while (1)
  {
    if(PackageBufferCount(&receivePackage) > 0)
    {
      StructPackage* currentPackage = PackageBufGet(&receivePackage);
      RecognizePackage(currentPackage);
    }
    
    if (stateWav == 1)
    {
      if (StateSendWav == 1)
      {
        SendWavFile();
      }
    }
    
    /* USER CODE END WHILE */
    
    /* USER CODE BEGIN 3 */
    
  }
  /* USER CODE END 3 */
  
}

/**
* @brief System Clock Configuration
* @retval None
*/
void SystemClock_Config(void)
{
  
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;
  
  /**Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  
  /**Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
    |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_USART3
    |RCC_PERIPHCLK_I2C2|RCC_PERIPHCLK_I2S;
  PeriphClkInitStruct.PLLI2S.PLLI2SN = 96;
  PeriphClkInitStruct.PLLI2S.PLLI2SP = RCC_PLLP_DIV2;
  PeriphClkInitStruct.PLLI2S.PLLI2SR = 2;
  PeriphClkInitStruct.PLLI2S.PLLI2SQ = 2;
  PeriphClkInitStruct.PLLI2SDivQ = 1;
  PeriphClkInitStruct.I2sClockSelection = RCC_I2SCLKSOURCE_PLLI2S;
  PeriphClkInitStruct.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  
  /**Configure the Systick interrupt time 
  */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
  
  /**Configure the Systick 
  */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
  
  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
* @brief  This function is executed in case of error occurrence.
* @param  file: The file name as string.
* @param  line: The line in file as a number.
* @retval None
*/
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
* @brief  Reports the name of the source file and the source line number
*         where the assert_param error has occurred.
* @param  file: pointer to the source file name
* @param  line: assert_param error line source number
* @retval None
*/
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
  tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
